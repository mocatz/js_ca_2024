---
marp: true
---

# 10 Functions

---

## Simple functions

```js
function sayHi() {
  console.log("hi");
}

sayHi();
```

---

## Function with parameters

```js
function add(a, b) {
  console.log(`Result : ${a} + ${b} = ${a + b}`);
}

add(5, 2);
```

---

## Pure function

> A function can be called pure if it returns the same output given the same input every time you call it, doesn't consume or modify other resources internally, and doesn't change its inputs.

## [Pure Functions](https://dev.to/alexkhismatulin/pure-functions-explained-for-humans-1j3c)

---

```js
let a = 5;
let b = 2;
function add() {
  return a + b;
}

console.log("result: ", add());
b = 5;
console.log("result: ", add());
```

```js
let a = 5;
let b = 2;
// -- Pure function do not access outer scope
function add(a, b) {
  return a + b;
}
let result = add(a, b);
console.log("result: ", result);
```

---

## scope

```js
let a = 10;
function getA() {
  let a = 100;
  console.log("a is: ", a);
  return a;
}
console.log("getA: ", getA());
```

```js
let a = 10;
function getA() {
  console.log("a is: ", a);
  let a = 100;
  return a;
}
console.log("getA: ", getA());
```

---

## Function expression

```js
const add = function (a, b) {
  return a + b;
};
console.log(add(5, 2));
// 7
```

```js
const add = (a, b) => a + b;
console.log(add(5, 2));
// 7
```

---

## High order function

> Higher-order functions. Functions that operate on other functions, either by taking them as arguments or by returning them, are called higher-order functions.

---

## Closures

[Closures](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures)

```js
function sayHi(greeting) {
  function hiTo(name) {
    return `${greeting} ${name}!`;
  }
  return hiTo;
}

const hiInFrench = sayHi("Salut");
const user1Greeting = hiInFrench("Paul");
const user2Greeting = hiInFrench("Tom");
console.log("user1Greeting: ", user1Greeting);
console.log("user2Greeting: ", user2Greeting);
```

---

```js
let count = "global scope";
function counterInit() {
  let count = 0;
  return function incr() {
    count++;
    console.log("counter value: ", count);
    return count;
  };
}

console.log("count: ", count);
const mainCounter = counterInit();
mainCounter();
mainCounter();
count = "Reassingn count global scope";
const counterState = mainCounter();
console.log("mainCounter value: ", counterState);
mainCounter();
console.log("count: ", count);
console.log("mainCounter value: ", counterState);
```

---

```js
function counterInit() {
  let count = 0; // private
  function incr() {
    return count++;
  }
  function decr() {
    return count--;
  }
  function getCount() {
    return count;
  }
  return { incr, decr, getCount };
}

const counter = counterInit();
counter.incr();
counter.incr();
console.log("counter value: ", counter.getCount());
```

---

## Obj & function

```js
const score = {
  score: 0,
  add() {
    this.score++;
    return this.score;
  },
  sub() {
    if (this.score <= 0) {
      this.score = 0;
    } else {
      this.score--;
    }
    return this.score;
  },
};

score.add();
```
