---
marp: true
---

# JS AND DOM MANIPULATION

---

You can acces elements in a web page
change there content, style, class, delete them.

```js
.getElementById
.getElementByTagName
.querySelector
```

---

## exercice

From the console:

- Go to a web page
- select an element
- change his value
- change his style

---

various way to get an element various type of data

- `let h1a = document.getElementsByTagName('h1')`
- `let h1b = document.querySelector('h1')`

---
