---
marp: true
---

# 9 Array helpers

---

## list of most common helpers

- map
- filter
- find
- findIndex
- reduce

---

## Map

```js
const numbers = [1, 2, 3];

const numberPlusTwo = numbers.map((num) => {
  return num + 2;
});
console.log("numberPlusTwo: ", numberPlusTwo);
console.log("numbers: ", numbers);
```

```js
const response = [
  { name: "John", num: 23 },
  { name: "Sarah", num: 38 },
  { name: "Tom", num: 32 },
];

const normalize = response.map((item) => {
  return { ...item, company: "CA" };
});
console.log("normalize: ", normalize);
console.log("response: ", response);
```

---

## Filter

```js
const response = [
  { name: "John", num: 23 },
  { name: "Sarah", num: 38 },
  { name: "Tom", num: 32 },
];

const moreThan30 = response.filter((item) => {
  return item.num >= 30;
});
console.log("moreThan30: ", moreThan30);
console.log("response: ", response);
```

---

## Find

```js
const response = [
  { name: "John", num: 23 },
  { name: "Sarah", num: 38 },
  { name: "Tom", num: 32 },
];

const itemMatch = response.find((item) => {
  return item.num === 38;
});
console.log("itemMatch: ", itemMatch);
```

---

## Find Index

```js
const response = [
  { name: "John", num: 23 },
  { name: "Sarah", num: 38 },
  { name: "Tom", num: 32 },
];

const itemIndexMatch = response.findIndex((item) => {
  return item.num === 38;
});
console.log("itemIndexMatch: ", itemIndexMatch);
```

---

## Reduce

```js
const response = [
  { name: "John", num: 3 },
  { name: "Sarah", num: 5 },
  { name: "Tom", num: 2 },
];

const numSum = response.reduce((acc, cur) => {
  return acc + cur.num;
}, 0);
console.log("numSum: ", numSum);
```

---
