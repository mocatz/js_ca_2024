---
marp: true
---

# 7 destructuring And spread

---

Destructuring is a useful way to get value of an object or array. You can do it in more "imperative way", but the declarative way is much cleaner.

---

```js
// IMPERATIVE APPROACH
function data() {
  return [1, 2, 3];
}

let tmp = data();
let first = tmp[0];
let second = tmp[1];
let third = tmp[2];
```

```js
// DECLARATIVE APPROACH
function data() {
  return [1, 2, 3];
}

let [first, second, third] = data();
```

---

```js
const obj = {
  name: "John",
  num: 23,
};

const name = obj.name;
const num = obj.num;
```

```js
const obj = {
  name: "John",
  num: 23,
};

const { name, num } = obj;
```

---

## useful to destructure params in a function

```js
const obj = {
  name: "John",
  num: 23,
};

function sayHi({ name }) {
  return `Hi ${name}`;
}

sayHi(obj);
// -> 'Hi John'
```

---

## Destructuring array of objects

```js
// -- ES5
const users = [
  { name: "John", num: 23 },
  { name: "Sarah", num: 32 },
  { name: "Eric", num: 43 },
];

const numOne = users[0].num;
console.log("numOne: ", numOne);
```

```js
// -- ES6
const users = [
  { name: "John", num: 23 },
  { name: "Sarah", num: 32 },
  { name: "Eric", num: 43 },
];

const [{ num: numOne }] = users;
console.log("numOne: ", numOne);
```

---

```js
let data = [
  { name: "toto", email: "t@test.com" },
  { name: "Zozo", email: "z@test.com" },
];

let [{ name: name1, email: email1 }, { name: name2, email: email2 }] = data;

console.log("name1: ", name1);
console.log("name2: ", name2);
```

---

## Spread operator

```js
let a = [1, 2, 4];
let b = [5, 6, 7];
let c = [...a, ...b];
console.log(c);
// [1, 2, 3, 4, 5, 6, 7];
```

---

## merge objects

```js
const obj1 = {
  name: "John",
  num: 23,
};

const obj2 = {
  color: "green",
  num: 32,
};

const fullObj = { ...obj1, ...obj2 };
console.log("fullObj: ", fullObj);
```

**Warning** Order is important!

---
