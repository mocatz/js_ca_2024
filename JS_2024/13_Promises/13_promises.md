---
marp: true
---

# Promise

---

[promises](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises)

> A Promise is an object representing the eventual completion or failure of an asynchronous operation.

---

```js
function demoPromise() {
  return new Promise((resolve) => {
    setTimeout(() => {
      console.log("hello");
      resolve("demoPromise resolved");
    }, 10000);
  });
}

let demo = demoPromise();
console.log("demo: ", demo);
```

---

```js
function demoPromise() {
  return new Promise((resolve) => {
    setTimeout(() => {
      console.log("hello");
      resolve(99999);
    }, 10000);
  });
}
let value = 10;
console.log("value: ", value);
demoPromise().then((response) => (value = response));
console.log("value: ", value);
```

---

Promise have 3 states

- pending
- fullfiled
- rejected

---

```js
let promise = new Promise(function (resolve, reject) {
  resolve("I am done");
});
```

```js
let promise = new Promise(function (resolve, reject) {
  reject(new Error("Something is not right!"));
});
```

---

## Demo with an API

```js
const ALL_POKEMONS_URL = "https://pokeapi.co/api/v2/pokemon?limit=50";

async function getPokemon(url) {
  const response = await fetch(url);
  const data = await response.json();
  return data;
}

let pokeList = [];

getPokemon(ALL_POKEMONS_URL).then((response) => {
  pokeList = response.results;
});

console.log("pokeList: ", pokeList);
```

---
