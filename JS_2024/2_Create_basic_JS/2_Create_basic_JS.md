# 2 Create basic JS

---

## Add script tag

- The most simple way to write JS in your HTML file is to use the `<script></script>` tag
- Any valid js is allowed here
- But for large script it's not very suitable

---

## Import js file (common)

_jsFile/_

- A more common way is to separate the js file and import it
- the import can be placed anywhere but often you need to access to the DOM
  so the DOM should be loaded to access any elements
- So the script should be at the end
- But you can use the `defer` attribute to tell 'Hey do it after the DOM loaded'

---
