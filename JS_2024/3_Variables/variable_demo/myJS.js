console.log("--- VARIABLES ---");

// -- 1
/*
var name1 = "John";
let name2 = "John";
const name3 = "John";

console.log('name1: ', name1);
console.log('name2: ', name2);
console.log('name3: ', name3);
*/

// -- 2
/*
var name1 = "John";
let name2 = "John";
const name3 = "John";

console.log("name1: ", name1);
console.log("name2: ", name2);
console.log("name3: ", name3);

name1 = 123;
name2 = "test";
name3 = "Aie!";

console.log("name1: ", name1);
console.log("name2: ", name2);
console.log("name3: ", name3);
*/

// -- 3
/*
let array1 = [1, 2, 3];
const array2 = ["a", "b", "c"];

console.log("array1: ", array1);
console.log("array2: ", array2);

array1[0] = 99999;
array2[0] = "Aie!";

console.log("array1: ", array1);
console.log("array2: ", array2);
*/
