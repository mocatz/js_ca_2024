# 3 variables

To define a variable there is 3 ways

- var
- let (es6)
- const (es6)

```
var name1 = 'John';
let name2 = 'Sarah';
const name3 = 'Sarah';
```

---

- Modern JS mainly use `let` & `const`
- But `var` still valid and behind there is a scope notion

---

A good article and ref:
['Var, Let, and Const – What's the Difference?'](https://www.freecodecamp.org/news/var-let-and-const-whats-the-difference/)

---

**WARNING**
Indeed const can not be reassign, but array and objects values can be changed.

```js
const myArray = [1, 2, 3, 4];
console.log(myArray);
myArray[0] = "oups";
console.log(myArray);
```

---
