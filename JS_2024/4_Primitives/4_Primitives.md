---
marp: true
---

# 4 Primitives

---

- JS get 7 primitives
- Primitives are at the core of the language
- They are not objects: no methods or properties

---

- **string** ('Hey there!')
- **number** (123456)
- bigint
- **boolean** (true/false)
- **undefined**
- symbol
- **null**

---

[Primitive](https://developer.mozilla.org/en-US/docs/Glossary/Primitive)
[String Primitive](https://developer.mozilla.org/en-US/docs/Glossary/String)
[String Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)

---

## String

```
let text = 'hi there!';
```

> In JavaScript, a String is one of the primitive values and the String object is a wrapper around a String primitive.

```
let text = 'This sentence contain the 42 number as you can see!';
text.includes('42');
// <- true
```

---

## String properties an methods (most used)

**properties:**

- length()

**methods:**

- includes()
- replace()
- split()
- toLowerCase()
- toUpperCase()
- trim()

---

You can concat strings:

```js
let firstname = "John";
let lastName = "John";
let fullName = firstname + " " + lastname;
```

or

```js
let firstname = "John";
let lastName = "John";
let fullName = `${firstname} ${lastname}`;
```

---

## Number

```
let year = 2024;
```

```
let year = 2024;
console.log('previous year: ', year - 1)
//  2023

```

---

## String properties an methods (most used)

**properties:**

- length()

**methods:**

- isInteger()
- isNaN()
- parseFloat()
- parseInt()
- toString()

---

## Boolean

```
let isLogged = true;
```

```
let isLogged = true;
console.log('is Logged: ', isLogged)
//  true

```

---

## undefined & null

Often undefined mean the js engine can not access to a variable with this name: not declared or declared after the execution.

```
console.log('my name: ', myName);
let myName =  'John';
```

But it could be a variable declared without any value attached.

```
let myName;
console.log('my name: ', myName);
```

Null is used to explicity say 'i don't know the value'

```
let myName = null;
console.log('my name: ', myName);
myName = 'John'
console.log('my name: ', myName);
```

---
