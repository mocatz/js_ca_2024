# Formation Javascript

[Project link](https://drive.google.com/drive/folders/1VVHFNZlie-Jh-ZXBfWNzPwhdTK_mUT64?usp=sharing)

1. Javascript
2. Create basic JS
3. Variable (var, let, const)
4. JS Primitive
5. Conditions / operators
6. Array & Objects
7. Destructuring array & Objects

- exo

---

8. Loop (for, forEach ... )
9. Array map filter find findIndex reduce ...
10. Functions (declaration, expression, closure)
11. JS and the DOM (ex toDoList)
12. Other Data structures Map, Set, And API Date, Math

- https://developer.mozilla.org/en-US/docs/Web/API

---

13. Promises (1h)
14. Asynchrone (callBack, then, async/await) (2h)
15. Fetch (0.5)
16. Exercices (2/3h)
17. Appendix (IIFE)

## Exercices

## 1 Create a common filter

**in**
[{name: 'john', registered: true}, {name: 'Sarah', registered: false}, {name: 'Claire', registered: true}, {name: 'Igor', registered: false}];
**out**
[{name: 'Sarah', registered: false}, {name: 'Igor', registered: false}]

```js
function superFilter(objList, objKeyToSearch, valueToMatch) {
  // -- Your code here
  return filteredList;
}
```

## 2 Create a grouper

From

```js
[
{brand: 'ford', model: 'AZER'}
{brand: 'Toyota', model: 'AZER'}
{brand: 'ford', model: 'AZER'}
]
```

to

```js
{
ford: [
{brand: 'ford', model: 'AZER'},
{brand: 'ford', model: 'AZER'}
],
toyota: [
{brand: 'Toyota', model: 'AZER'}
]
}
```

## 3 create an object sorter

```js
function sortCollection(collection, keyRef) {}
```

## 4 To do list

create a to list

```js
{
  task: 'Call Fred',
  ts: 1234567890,
  state: 'TODO'
}
```

1. an input field for the task to add
2. A button to add it to the toDoList
3. display the todo list
4. Add a button **DONE** that change css on the task
5. add a button to delete the task
6. add sorter task by 'task' / 'ts'
7. add filter by DONE / TODO
8. add a checkbox to autodelete task after x seconds
9. Add state doing

```

```
