---
marp: true
---

# Array [] and Object {}

---

## Array

An array can store a list of anything

```js
let myArray = ["hi", 123, null, { name: "John" }, [1, 2, 3]];
```

You access the value of specific index:

```js
console.log(myArray[1]); // 123
console.log(myArray[4][1]); // 2
```

---

### common operation on array

Add items

```js
let myArray = [1, 2, 3];
myArray.push(4); // at end
myArray.unshift(0); // at start
console.log(myArray);
```

Delete items

```js
let myArray = [1, 2, 3];
myArray.pop(); // at end
myArray.shift(); // at start
console.log(myArray);
```

---

## Change a value

```js
// Capitalize 'greg'
let myArray = ["Sarah", "greg", "John"];
myArray[1] = myArray[1].charAt(0).toUpperCase() + myArray[1].slice(1);
console.log(myArray);
```

---

### reverse

```js
const myArray = [5, 4, 3, 2, 1, 0];
let reversed = myArray.reverse();
console.log("reversed: ", reversed);
console.log("myArray: ", myArray);
```

**Warning** these array methods change the the content of the array
Array values arre passed by **REFERENCE**

---

### Sort

```js
const myArray = [1, 3, 2, 5, 4];
const sorted = myArray.sort();
console.log("myArray sorted: ", sorted);
console.log("myArray original: ", myArray);
```

---

[Array sort](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort)

> The sort() method returns a reference to the original array, so mutating the returned array will mutate the original array as well.

---

## Sort string

```js
let names = ["Bernard", "Zoro", "alice"];
names.sort();
console.log("names sorted: ", names);
```

```js
let names = ["Bernard", "Zoro", "Alice"];
names.sort();
console.log("names sorted: ", names);
```

```js
const array1 = [1, 30, 4, 21, 100000];
array1.sort();
console.log(array1);
```

[Sort mdm doc](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort)

---

## sort string with function

```js
let names = ["Bernard", "Zoro", "alice"];
names.sort((a, b) => {
  let aLower = a.toLowerCase();
  let bLower = b.toLowerCase();
  if (bLower > aLower) {
    return -1;
  } else if (bLower < aLower) {
    return 1;
  } else {
    return 0;
  }
});
console.log("names sorted: ", names);
```

---

Tips:
Array type return 'object'.
To check if an array is an array use Array.isArray;

```js
let myArray = [1, 2, 3];
typeof myArray; // 'object'
Array.isArray(myArray); // true
```

---

## Objects {}

Most simple form

```js
let film = {
  name: "Blade Runner",
  director: "	Ridley Scott",
  music: "Vangelis",
  year: 1982,
  time: 117,
};
```

[Object initializer](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Object_initializer)

---

## Access value by key . []

```js
film.name; // 'Blade Runner'
film["name"]; // 'Blade Runner'
// --  The [] access notation is useful with variable
const searchItemBy = "year";
film[searchItemBy]; // 1982;
```

---

## add a property

```js
let film = {
  title "Blade Runner",
  director: "	Ridley Scott",
  music: "Vangelis",
  year: 1982,
  time: 117,
};

film.watched = true;
console.log('film: ', film);
```

---

## Update a Value

```js
let film = {
  title "Blade Runner",
  director: "	Ridley Scott",
  music: "Vangelis",
  year: 1982,
  time: 117,
};

film.time = 120;
console.log('film: ', film);
```

---

## Exercice

Fix the empty space in the film director:

```js
let film = {
  title "Blade Runner",
  director: "	Ridley Scott",
  music: "Vangelis",
  year: 1982,
  time: 117,
};
// -- Your code here
console.log('film: ', film);
film.director == "Ridley Scott"
// -> true
```

---

## Most often you will get an array of objects

```js
let films = [
  {
    name: "Blade Runner",
    director: "	Ridley Scott",
    music: "Vangelis",
    year: 1982,
    time: 117,
  },
  {
    name: "Oblivion",
    director: "Joseph Kosinski",
    music: "M83",
    year: 2013,
    time: 124,
  },
];
```
