---
marp: true
---

# 5 Condition & operators

---

# if else

```
let score = 99;
if (score > 80) {
  console.log('Great')
} else if (score >= 50) {
  console.log('OK')
} else {
  console.log('KO')
}
```

When you have more than if / else if / else,
maybe consider an other way to make your conditional statement to avoid if hell.

---

# switch

```
let score = 80

switch (true) {
  case (score == 100):
    console.log('Top');
    break;
  case score >= 80:
    console.log('Great');
    break;
  case score >= 50:
    console.log('Good job');
    break;
  case score > 25:
    console.log('Retry later');
    break;
  default:
    console.log('Start at chapter 1');
}
```

---

# switch(2 )

```
const STATUS = 'OFF'

switch (STATUS) {
  case 'ON':
    console.log('Is running');
    break;
  case 'OFF':
    console.log('Please start the machine');
    break;
  default:
    console.log('Something wrong please restart');
}
```

---

## Math operators

`+ - * / %`

**Warning**
A common pitfall: number divided by 0 result in `Infinity`.

```
80 / 0
// Infinity
-80 / 0
// -Infinity
```

---

```
let a = 23 + 245;
let b = 23 - 245;
let c = 23 * 245;
let d = 1000 * 250;
let e = 9 % 8;
```

---

## compare `==` & `===` (coercion)

Aka equal and strictly equal

[Equality Operators](https://262.ecma-international.org/5.1/#sec-11.9);
[Equality comparisons and sameness](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Equality_comparisons_and_sameness)

```
1 == 1 // true
'1' == 1 // true
'1' === 1 // false
```

> "peut être que 1 + 1 = 11 et ça c'est beau" jcvd
> _coercion is the case for the `'1' + 1 = 11`;_

---

## compare `!==`

Of course there is not equal

```
1 !== 1 // true
'demo' !== 1 // true
'demo' !== 'Demo' // true
```

> "peut être que 1 + 1 = 11 et ça c'est beau" jcvd
> _coercion is the case for the `'1' + 1 = 11`;_

---

## AND && (logical operator)

```js
let a = "toto";
let b = 3;
a && b;
// 3
Boolean(a && b);
// true
0 && "toto";
// 0
false && "toto";
// false
false && true;
// false
```

---

## OR ||

```js
let a = "toto";
let b = 3;
a || b;
// toto
Boolean(a || b);
// true
0 || "toto";
// 'toto'
false || "toto";
// 'toto'
false || true;
// true
```
